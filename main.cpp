#include <iostream>
#include <condition_variable>
#include <atomic>
#include <vector>
#include <thread>

using namespace std;

struct TheCVStruct final {
    TheCVStruct() {
        cout << "TheCVStruct()" << endl;
    }

    ~TheCVStruct() {
        cout << "~TheCVStruct()" << endl;
    }

    mutex mtx;
    condition_variable cv;
    atomic_size_t thread_count;
};

class RNG {
public:
    RNG() : data("default") {
        cout << "RNG()\n";
    }

    virtual ~RNG() {
        cout << "~RNG()\n";
    }

    string get() const {
        return data;
    }

    void set(const string val) {
        data = val;
    }

private:
    string data;
};

void doAnotherMagic(function<RNG*(string)> &&f, string &arg, unique_ptr<RNG> &rng, shared_ptr<TheCVStruct> &cvStruct) {
    unique_ptr<RNG> tmp(f(arg));
    tmp->set(arg);
    if (tmp != nullptr && rng.get() == nullptr) {
        unique_lock<mutex> lk(cvStruct->mtx);
        if (rng.get() == nullptr) {
            cout << "valid object created. Others are reundant and will be released without any usage\n;"
                            "notifying cv...\n";
            rng = move(tmp);
            cvStruct->cv.notify_one();
        }
    }
    cvStruct->thread_count.fetch_sub(1);
    if (cvStruct->thread_count.load() == 0) {
        cvStruct->cv.notify_one();
        cout << "ALL DETACHED THREADS ARE JOINED\n";
    }
}

void doMagic(function<RNG*(string)> &&f, string &arg, unique_ptr<RNG> &ptr, shared_ptr<TheCVStruct> &cvStruct) {

    thread t(&doAnotherMagic, move(f), ref(arg), ref(ptr), ref(cvStruct));

    if (t.joinable()) {
        t.detach();
    }
}

class A {
public:
    A() {
        cout << "A()\n";
    }
    virtual ~A() {
        cout << "~A()\n";
    }

    RNG* _what(string str1) {
        cout << "_what, param = " << str1 << endl;
        return new RNG();
    }

    RNG* _foo(string str1, string str2) {
        cout << "_foo, param = " << str1 << endl;
        return new RNG();
    }

    unique_ptr<RNG> doIt() {
        vector<string> vec = {
                "abba",
                "abbqwea",
                "abbqra",
                "abbgt4a",
                "ab3ba",
                "abt4rba"
        };
        unique_ptr<RNG> res(nullptr);
        shared_ptr<TheCVStruct> cvStruct(new TheCVStruct());
        cvStruct->thread_count = vec.size() + 1;

        thread waiter([&]() {
            unique_lock<mutex> lk(cvStruct->mtx);
            cvStruct->cv.wait(lk, [&]() { return res.get() != nullptr || cvStruct->thread_count.load() == 0; });
        });

        auto what = std::bind(&A::_what, this, placeholders::_1);
        auto foo = std::bind(&A::_foo, this, placeholders::_1, string());

        for (auto & arg : vec) {
            doMagic(move(foo), arg, res, cvStruct);
        }
        string arg = "zzz";
        doMagic(move(what), arg, res, cvStruct);

        if (waiter.joinable())  {
            waiter.join();
        }
        return res;
    }

};

int main() {
    A a;
    unique_ptr<RNG> p = nullptr;
    p = a.doIt();
    cout << "get() = " << p->RNG::get() << endl;
    this_thread::sleep_for(chrono::seconds(3));
    return 0;
}